ifneq ($(TARGET_GAPPS_ARCH),arm)
ifneq ($(TARGET_GAPPS_ARCH),arm64)
TARGET_GAPPS_ARCH := arm64
endif
endif

$(call inherit-product, vendor/xiaomi/pps/common-blobs.mk)

# framework
PRODUCT_PACKAGES += \
    com.google.android.maps

PRODUCT_PACKAGES += \
    com.google.android.dialer.support

# System app
PRODUCT_PACKAGES += \
    GoogleExtShared

# Product app
PRODUCT_PACKAGES += \
    GoogleContactsSyncAdapter \
    GoogleTTS \
    SoundPickerPrebuilt

# Product priv-app
PRODUCT_PACKAGES += \
    AndroidMigratePrebuilt \
    CarrierServices \
    ConfigUpdater \
    Phonesky \
    PrebuiltGmsCoreRvc \
    SetupWizardPrebuilt

# System-Ext priv-app
PRODUCT_PACKAGES += \
    GoogleFeedback \
    GoogleOneTimeInitializer \
    GoogleServicesFramework

# Google legal
PRODUCT_PRODUCT_PROPERTIES += \
    ro.url.legal=http://www.google.com/intl/%s/mobile/android/basic/phone-legal.html \
    ro.url.legal.android_privacy=http://www.google.com/intl/%s/mobile/android/basic/privacy.html

# Google Play services configuration
PRODUCT_PRODUCT_PROPERTIES += \
    ro.com.google.clientidbase=android-google \
    ro.error.receiver.system.apps=com.google.android.gms \
    ro.atrace.core.services=com.google.android.gms,com.google.android.gms.ui,com.google.android.gms.persistent

